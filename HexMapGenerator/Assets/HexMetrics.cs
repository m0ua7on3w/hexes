﻿using UnityEngine;
public static class HexMetrics 
{
	private class Cube
	{
		public int x, z, y;
		public Cube(int x, int y, int z){
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	private class Axial
	{
		public int x, z;
		public Axial( int x, int z){
			this.x = x;
			this.z = z;
		}
	}
	public const float outerRadius = 1f;
	public const float innerRadius = outerRadius * 0.866025404f;
	public const float hexHeight = 1f;
	// Adding them clock-wise from Most up corner to the most up-left
	public static Vector3[] topCorners = {
		new Vector3(0f, 0f, outerRadius),
		new Vector3(innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(0f, 0f, -outerRadius),
		new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
	};

	public static Vector3 CoordinatesToWorldPosition(int x, int y, int z)
	{

		var x1 = outerRadius * Mathf.Sqrt(3) * (x + 0.5 * (z&1));
    	var y1 = outerRadius * 3/2 * z;
		Vector3 offset = new Vector3(0f,0f,0f);
		Vector3 WorldPosition = new Vector3
		( (float)x1,//outerRadius * Mathf.Sqrt(3) * (x + z/2),//(x + (z * 0.5f) - (z / 2)) * (innerRadius * 2f),
		y,
		 (float)y1);//outerRadius * 3/2 * z);//z * outerRadius * 1.5f);
		return WorldPosition;
	}

	public static WorldPos WorldPositionToCoordinates(Vector3 vector)
	{

		float t1 = vector.x / innerRadius;
		float t2 = vector.z / (outerRadius*2);

		/*float r = Mathf.Floor( ( Mathf.Floor(vector.z / .5f ) + Mathf.Floor(t2-t1) + 2 ) / 3);
		float q = Mathf.Floor( (Mathf.Floor(t1-t2) + Mathf.Floor(t1+t2) + 2) / 3);
		*/
		var q = (vector.x * Mathf.Sqrt(3)/3 - vector.z / 3) / outerRadius;
    	var r = vector.z * 2/3 / outerRadius;
		WorldPos worldPos = new WorldPos ( 
			Mathf.RoundToInt(q)
			, (int)vector.y
			, Mathf.RoundToInt(r));

		Axial axe = cube_to_oddr(Cube_round(axial_to_cube(new Axial((int)q, (int)r))));
		
		return new WorldPos(axe.x, Mathf.RoundToInt(vector.y), axe.z);
	}

	

	private static Cube axial_to_cube(Axial axial){
		var x = axial.x;
    	var z = axial.z;
    	var y = -x-z;
	    return new Cube(x, y, z);
	}

	private static Axial cube_to_axial(Cube cube){
		var q = cube.x;
    	var r = cube.z;
	    return new Axial(q,r);
	}

	private static Cube Cube_round(Cube cube){
		var rx = Mathf.Round(cube.x);
		var ry = Mathf.Round(cube.y);
		var rz = Mathf.Round(cube.z);

		var x_diff = Mathf.Abs(rx - cube.x);
		var y_diff = Mathf.Abs(ry - cube.y);
		var z_diff = Mathf.Abs(rz - cube.z);

		if (x_diff > y_diff && x_diff > z_diff)
			rx = -ry-rz;
		else if (y_diff > z_diff)
			ry = -rx-rz;
		else
			rz = -rx-ry;

		return new Cube((int)rx, (int)ry, (int)rz);
	}

	private static Axial cube_to_oddr(Cube cube){
		var col = cube.x + (cube.z - (cube.z&1)) / 2;
		var row = cube.z;
		return new Axial(col, row);
	}

}
