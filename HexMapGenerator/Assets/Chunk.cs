﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Store the data of their contents and create a mesh of their contained voxels for rendering and collisions.
/// </summary>

/// Included so that when we create a GameObject with this script they are automatically added.
/// We need those components to render the mesh and create the collider for the chunk.
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class Chunk : MonoBehaviour {

	/// TODO: This is a 3-dim chunk, for now i'll keep it ( tutorial-safe ), later we should change it for a 2-dim chunk with eight per Hex if we need to
	/// TODO: No idea how we will handle the y-axis, that's also a problem for later
	
	public static int chunkSize = 16; 
	Hex[ , , ] hexes = new Hex[chunkSize, chunkSize, chunkSize]; 
	public bool update = true;
	
	MeshFilter meshFilter;
	MeshCollider meshCollider;

	public World world;
	public WorldPos worldPosition;

	void Start () { 
		meshFilter = gameObject.GetComponent<MeshFilter>();
        meshCollider = gameObject.GetComponent<MeshCollider>();


	}
	void Update () {
		if (update)
		{
			update = false;
			UpdateChunk();
		}
	 }

	public Hex GetHex(int x, int y, int z)
	{
		if(InRange(x) && InRange(y) && InRange(z))
			return hexes[x, y, z];
		return world.GetHex(worldPosition.x + x, worldPosition.y + y, worldPosition.z + z);
	}

	public void SetHex(int x, int y, int z, Hex hex)
     {
         if (InRange(x) && InRange(y) && InRange(z))
         {
             hexes[x, y, z] = hex;
         }
         else
         {
             world.SetHex(worldPosition.x + x, worldPosition.y + y, worldPosition.z + z, hex);
         }
     }

	public static bool InRange(int index)
	{
		if (index < 0 || index >= chunkSize)
			return false;

		return true;
	}

	/// <summary>
	/// Updates the chunk based on its contents
	/// </summary>
	void UpdateChunk()
	{
		MeshData meshData = new MeshData();
         for (int x = 0; x < chunkSize; x++)
         {
             for (int y = 0; y < chunkSize; y++)
             {
                 for (int z = 0; z < chunkSize; z++)
                 {
                     meshData = hexes[x, y, z].HexData(this, x, y, z, meshData);
                 }
             }
         }
         RenderMesh(meshData);
	}

	/// <summary>
	/// Sends the calculated mesh information
	/// to the mesh and collision components
	/// </summary>
	void RenderMesh(MeshData meshData)
	{
		meshFilter.mesh.Clear();
		meshFilter.mesh.vertices = meshData.vertices.ToArray();
		meshFilter.mesh.triangles = meshData.triangles.ToArray();
		meshFilter.mesh.uv = meshData.uv.ToArray();
		meshFilter.mesh.RecalculateNormals();

		meshCollider.sharedMesh = null;
		Mesh mesh = new Mesh();
		mesh.vertices = meshData.colVertices.ToArray();
		mesh.triangles = meshData.colTriangles.ToArray();
		mesh.RecalculateNormals();

		meshCollider.sharedMesh = mesh;
	}
}
