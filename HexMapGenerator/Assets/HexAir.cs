﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexAir : Hex {

	public HexAir() : base() { }

	public override MeshData HexData(Chunk chunk, int x, int y, int z, MeshData meshData)
	{
		return meshData;
	}
	public override bool IsSolid(Hex.Direction Direction)
	{
		return false;
	}
}
