﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Terrain {

	public static WorldPos GetHexPosition(Vector3 pos)
	{
		HexMetrics.WorldPositionToCoordinates(pos);
		WorldPos hexPosition = new WorldPos (Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.z));
		return HexMetrics.WorldPositionToCoordinates(pos);
	}

	public static WorldPos GetHexPosition(RaycastHit hit, bool adjacent = false)
     {
		 Debug.Log(hit.point);
         Vector3 pos = new Vector3(
             MoveWithinHex(hit.point.x, hit.normal.x, adjacent),
             MoveWithinHex(hit.point.y, hit.normal.y, adjacent),
             MoveWithinHex(hit.point.z, hit.normal.z, adjacent)
             );
         return GetHexPosition(pos);
     }

     static float MoveWithinHex(float pos, float norm, bool adjacent = false)
     {
          if (pos - (int)pos == 0.5f || pos - (int)pos == -0.5f)
          {
              if (adjacent)
              {
                  pos += (norm / 2);
              }
              else
              {
                  pos -= (norm / 2);
              }
          }
  
         return (float)pos;
     }

	 public static bool SetHex(RaycastHit hit, Hex hex, bool adjacent = false)
     {
         Chunk chunk = hit.collider.GetComponent<Chunk>();
         if (chunk == null)
             return false;
  
         WorldPos pos = GetHexPosition(hit, adjacent);
  
         chunk.world.SetHex(pos.x, pos.y, pos.z, hex);
  
         return true;
     }

	 public static Hex GetHex(RaycastHit hit, bool adjacent = false)
     {
         Chunk chunk = hit.collider.GetComponent<Chunk>();
         if (chunk == null)
             return null;
  
         WorldPos pos = GetHexPosition(hit, adjacent);
  
         Hex hex = chunk.world.GetHex(pos.x, pos.y, pos.z);
  
         return hex;
     }
	
}
