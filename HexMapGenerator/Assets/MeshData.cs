﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provide an easy way to store mesh data.
/// </summary>
public class MeshData{

	/// Vertices are vector 3 positions in space that define the points or corners of every triangle in the mesh. 
	/// Every three entries in the triangles list defines one triangle and the entry itself is the index of a vector 3 in the vertices array
	public List<Vector3> vertices = new List<Vector3>();
	public List<int> triangles = new List<int>();

	/// The uv list is a vector 2 list of texture coordinates and there are two entries per triangle, 
	/// the coordinates of the lower left of the triangle and the upper right of the triangle.
	public List<Vector2> uv = new List<Vector2>();

	/// The col lists are the same as vertices and triangles but for use as the collider mesh 
	/// so that you can pass a different mesh for the rendering and the collider.
	public List<Vector3> colVertices = new List<Vector3>();
	public List<int> colTriangles = new List<int>();

	public bool useRenderDataForCollider;
	

	public MeshData (){ }

	public void AddTriangle (int tri) {
		triangles.Add(tri);
		if (useRenderDataForCollider)
		{
			colTriangles.Add(tri - (vertices.Count - colVertices.Count));
		}
	}

	public void AddQuadTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
	{
		int vertexIndex = vertices.Count;
		AddVertex(v1);
		AddVertex(v2);
		AddVertex(v3);
		AddVertex(v4);

		triangles.Add(vertexIndex);
		triangles.Add(vertexIndex + 1);
		triangles.Add(vertexIndex + 2);
		triangles.Add(vertexIndex );
		triangles.Add(vertexIndex + 2);
		triangles.Add(vertexIndex + 3);

		if(useRenderDataForCollider){
			int colVertexIndex = colVertices.Count - 4;
			
			colTriangles.Add(colVertexIndex);
			colTriangles.Add(colVertexIndex + 1);
			colTriangles.Add(colVertexIndex + 2);
			colTriangles.Add(colVertexIndex );
			colTriangles.Add(colVertexIndex + 2);
			colTriangles.Add(colVertexIndex + 3);
		}

	}

	public void AddHexaTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6 )
	{
		int vertexIndex = vertices.Count;
		AddVertex(v1);
		AddVertex(v2);
		AddVertex(v3);
		AddVertex(v4);
		AddVertex(v5);
		AddVertex(v6);

		triangles.Add(vertexIndex);
		triangles.Add(vertexIndex + 1);
		triangles.Add(vertexIndex + 2);

		triangles.Add(vertexIndex);
		triangles.Add(vertexIndex + 2);
		triangles.Add(vertexIndex + 5);

		triangles.Add(vertexIndex + 2);
		triangles.Add(vertexIndex + 3);
		triangles.Add(vertexIndex + 5);

		triangles.Add(vertexIndex + 5);
		triangles.Add(vertexIndex + 3);
		triangles.Add(vertexIndex + 4);

		if(useRenderDataForCollider){
			int colVertexIndex = colVertices.Count - 6;
			colTriangles.Add(colVertexIndex);
			colTriangles.Add(colVertexIndex + 1);
			colTriangles.Add(colVertexIndex + 2);

			colTriangles.Add(colVertexIndex);
			colTriangles.Add(colVertexIndex + 2);
			colTriangles.Add(colVertexIndex + 5);

			colTriangles.Add(colVertexIndex + 2);
			colTriangles.Add(colVertexIndex + 3);
			colTriangles.Add(colVertexIndex + 5);

			colTriangles.Add(colVertexIndex + 5);
			colTriangles.Add(colVertexIndex + 3);
			colTriangles.Add(colVertexIndex + 4);
		}
	}

	public void AddVertex(Vector3 vertex)
	{
		vertices.Add(vertex);
		if(useRenderDataForCollider)
		{
			colVertices.Add(vertex);
		}
	}
}
