﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class all other block types will inherit from
/// </summary>
public class Hex {

	public Hex() { }
	public Vector3 centerPosition = new Vector3(0f,0f,0f);

	const float tileSize = 0.25f;
	public struct Tile { public int x; public int y; }


	public virtual Tile TexturePosition(Direction direction)
	{
		Tile tile = new Tile();
		tile.x = 0;
		tile.y = 0;
		return tile;
	}

	public virtual Vector2[] QuadUVs(Direction direction)
	{
		Vector2[] UVs = new Vector2[4];
		Tile tilePos = TexturePosition(direction);
		UVs[2] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y);
		UVs[3] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y + tileSize);
		UVs[0] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y + tileSize);
		UVs[1] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y);
		return UVs;

	}

	public virtual Vector2[] HexUVs(Direction direction)
	{
		Vector2[] UVs = new Vector2[6];
		Tile tilePos = TexturePosition(direction);
		UVs[0] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y);
		UVs[1] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y + tileSize);
		UVs[2] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y + tileSize);
		UVs[3] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y);
		UVs[4] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y + tileSize);
		UVs[5] = new Vector2(tileSize * tilePos.x , tileSize * tilePos.y);

		return UVs;
	}

	public virtual MeshData HexData (Chunk chunk, int x, int y, int z, MeshData meshData)
	{
		meshData.useRenderDataForCollider = true;
		if (!chunk.GetHex(x, y + 1, z).IsSolid(Direction.down))
		{
			meshData = FaceDataUp(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x, y - 1, z).IsSolid(Direction.up))
		{
			meshData = FaceDataDown(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x - 1, y, z).IsSolid(Direction.east))
		{
			meshData = FaceDataWest(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x - 1, y, z + 1).IsSolid(Direction.southEast))
		{
			meshData = FaceDataNorthWest(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x + 1, y, z + 1).IsSolid(Direction.southWest))
		{
			meshData = FaceDataNorthEast(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x, y, z + 1).IsSolid(Direction.west))
		{
			meshData = FaceDataEast(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x - 1, y, z - 1).IsSolid(Direction.northEast))
		{
			meshData = FaceDataSouthWest(chunk, x, y, z, meshData);
		}

		if (!chunk.GetHex(x + 1, y, z - 1).IsSolid(Direction.northWest))
		{
			meshData = FaceDataSouthEast(chunk, x, y, z, meshData);
		}

		return meshData;
	}




	// TODO : Face data up and down could have 2 triangles instead of 6 ( or 3, we should optimize that)
	// TODO : I did that to understand how it works
	protected virtual MeshData FaceDataUp (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 // A triangle is made by 3 verts, in the faceUp we can do six triangles, each connected to the center and 2 corners of the hex ( corners of the hex are in HexMetrics.topCorners)
		Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		meshData.AddHexaTriangle(HexMetrics.topCorners[0] + position, HexMetrics.topCorners[1] + position, HexMetrics.topCorners[2] + position,
		 						HexMetrics.topCorners[3] + position, HexMetrics.topCorners[4] + position, HexMetrics.topCorners[5] + position);
		 meshData.uv.AddRange(HexUVs(Direction.up));
         return meshData;
     }
  
     protected virtual MeshData FaceDataDown (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddHexaTriangle(HexMetrics.topCorners[0] + decreasedHeight + position, HexMetrics.topCorners[1] + decreasedHeight + position, HexMetrics.topCorners[2] + decreasedHeight + position,
		 						HexMetrics.topCorners[3] + decreasedHeight + position, HexMetrics.topCorners[4] + decreasedHeight + position, HexMetrics.topCorners[5] + decreasedHeight + position);
		meshData.uv.AddRange(HexUVs(Direction.down));
         return meshData;
     }
  
	/// For each side-face we need two triangles, we'll do them in clock-wise order
	/// For this part try to imagine the hex in your mind, merge this with the knowledge of HexMetrics and you should understand
	/// 
	/// 1------------2
	/// |			 |
	/// |			 |
	/// |			 |
	/// |			 |
	/// 4------------3
	/// 
	/// We're basically joining corners like that : 2/3/4 and 4/1/2
	/// They should all have 
	/// meshData.AddTriangle(index, index-height, (index+1)-height)
	/// meshData.AddTriangle((index+1)-height, index+1, index)
	/// We could do a function, but it may be easier to read / understand how it works like that
     protected virtual MeshData FaceDataEast (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[1] + position, HexMetrics.topCorners[1] + decreasedHeight + position, HexMetrics.topCorners[2] + decreasedHeight + position, HexMetrics.topCorners[2] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.east));
         return meshData;
     }
  
     protected virtual MeshData FaceDataNorthEast (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[0] + position, HexMetrics.topCorners[0] + decreasedHeight + position, HexMetrics.topCorners[1] + decreasedHeight + position, HexMetrics.topCorners[1] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.northEast));
         return meshData;
     }

	  protected virtual MeshData FaceDataNorthWest (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 // This one is diferent, instead of index+1, we need the first corner 
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[5] + position, HexMetrics.topCorners[5] + decreasedHeight + position, HexMetrics.topCorners[0] + decreasedHeight + position, HexMetrics.topCorners[0] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.northWest));
         return meshData;
     }
  
     protected virtual MeshData FaceDataSouthEast (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[2] + position, HexMetrics.topCorners[2] + decreasedHeight + position, HexMetrics.topCorners[3] + decreasedHeight + position, HexMetrics.topCorners[3] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.southEast));
         return meshData;
     }

	 protected virtual MeshData FaceDataSouthWest (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[3] + position, HexMetrics.topCorners[3] + decreasedHeight + position, HexMetrics.topCorners[4] + decreasedHeight + position, HexMetrics.topCorners[4] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.southWest));
         return meshData;
     }
  
     protected virtual MeshData FaceDataWest (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
		 Vector3 position = HexMetrics.CoordinatesToWorldPosition(x,y,z);
		 Vector3 decreasedHeight = new Vector3(0f, -HexMetrics.hexHeight, 0f);
		 meshData.AddQuadTriangle(HexMetrics.topCorners[4] + position, HexMetrics.topCorners[4] + decreasedHeight + position, HexMetrics.topCorners[5] + decreasedHeight + position, HexMetrics.topCorners[5] + position );
		 meshData.uv.AddRange(QuadUVs(Direction.west));
         return meshData;
     }

	/// When we render voxels we don't need to render every side of every voxel. 
	/// Some faces will be impossible for the player to see because they are covered by other voxels. 
	/// In the case of Hex voxels we can always assume that a block adjacent to another the sides facing each other aren't visible,
	/// so we don't need to render them. It's simple enough to only render faces of blocks facing empty.


	// TODO: Remove up and down if unnecessary
	public enum Direction { east, northEast, northWest, southEast, west, southWest, up, down };


	public virtual bool IsSolid(Direction direction)
	{
		switch(direction)
		{
			case Direction.east:
				return true;
				case Direction.northEast:
				return true;
				case Direction.northWest:
				return true;
				case Direction.west:
				return true;
				case Direction.southEast:
				return true;
				case Direction.southWest:
				return true;
				// TODO: Remove up and down if unnecessary
				case Direction.up:
				return true;
				case Direction.down:
				return true;
		}
		return false;
	}
}

