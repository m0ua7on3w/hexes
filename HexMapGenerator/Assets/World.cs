﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

	public Dictionary<WorldPos, Chunk> chunks = new Dictionary<WorldPos, Chunk>();

	public GameObject chunkPrefab;
	// Use this for initialization
	void Start () {
		for (int x = -2; x < 2; x++)
		{
			for (int y = -1; y < 1; y++)
			{
				for (int z = -1; z < 1; z++)
				{
					CreateChunk(x * 16, y * 16, z * 16);
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CreateChunk(int x, int y, int z)
	{
		WorldPos worldPos = new WorldPos(x,y,z);

		GameObject newChunkObject = Instantiate ( chunkPrefab, HexMetrics.CoordinatesToWorldPosition(x,y,z), Quaternion.Euler(Vector3.zero)) as GameObject;
		newChunkObject.name = "("+x+"/"+y+"/"+z+")";

		Chunk newChunk = newChunkObject.GetComponent<Chunk>();

		newChunk.worldPosition = worldPos;
		newChunk.world = this;

		chunks.Add(worldPos,newChunk);

		
		for (int xi = 0; xi < 16; xi++)
		{
			for (int yi = 0; yi < 16; yi++)
			{
				for (int zi = 0; zi < 16; zi++)
				{
					if (true)
					{
						SetHex(x+xi, y+yi, z+zi, new HexGrass());
					} 
					else
					{
						SetHex(x + xi, y + yi, z + zi, new HexAir());
					}
				}
			}
		}
	}

	public Chunk GetChunk(int x, int y, int z)
	{
		WorldPos pos = new WorldPos();
		float multiple = Chunk.chunkSize;
		pos.x = Mathf.FloorToInt(x / multiple ) * Chunk.chunkSize;
		pos.y = Mathf.FloorToInt(y / multiple ) * Chunk.chunkSize;
		pos.z = Mathf.FloorToInt(z / multiple ) * Chunk.chunkSize;
		Chunk containerChunk = null;
		chunks.TryGetValue(pos, out containerChunk);

		return containerChunk;
	}

	public Hex GetHex(int x, int y, int z)
     {
         Chunk containerChunk = GetChunk(x, y, z);
         if (containerChunk != null)
         {
             Hex hex = containerChunk.GetHex(
                 x - containerChunk.worldPosition.x,
                 y -containerChunk.worldPosition.y,
                 z - containerChunk.worldPosition.z);
  
             return hex;
         }
         else
         {
             return new HexAir();
         }
  
     }

	 public void SetHex(int x, int y, int z, Hex hex)
     {
         Chunk chunk = GetChunk(x, y, z);
  
         if (chunk != null)
         {
             chunk.SetHex(x - chunk.worldPosition.x, y - chunk.worldPosition.y, z - chunk.worldPosition.z, hex);
             chunk.update = true;

			UpdateIfEqual(x - chunk.worldPosition.x, 0, new WorldPos(x - 1, y, z));
			UpdateIfEqual(x - chunk.worldPosition.x, Chunk.chunkSize - 1, new WorldPos(x + 1, y, z));
			UpdateIfEqual(y - chunk.worldPosition.y, 0, new WorldPos(x, y - 1, z));
			UpdateIfEqual(y - chunk.worldPosition.y, Chunk.chunkSize - 1, new WorldPos(x, y + 1, z));
			UpdateIfEqual(z - chunk.worldPosition.z, 0, new WorldPos(x, y, z - 1));
			UpdateIfEqual(z - chunk.worldPosition.z, Chunk.chunkSize - 1, new WorldPos(x, y, z + 1));
         }
     }

	 void UpdateIfEqual(int value1, int value2, WorldPos pos)
     {
         if (value1 == value2)
         {
             Chunk chunk = GetChunk(pos.x, pos.y, pos.z);
             if (chunk != null)
                 chunk.update = true;
         }
     }
}
